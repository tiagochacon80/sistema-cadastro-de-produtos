package modulo6_vetores_colecoes;

import java.util.HashSet;
import java.util.Set;

public class Aula02HashSet {

	public static void main(String[] args) {
		
		//Saber se os itens adicionado foram repetidos ou nao, se sim, ele retona false, senao ele retorna true
		Set<String> colecao = new HashSet<>();
		
		colecao.add("Carro");
		colecao.add("Moto");
		colecao.add("Barco");
		colecao.add("Aviao");
		System.out.println(colecao.add("Barco"));
		System.out.println(colecao.add("Carro�a"));
		
		System.out.println();
		
		for(String veiculo : colecao) {
			System.out.println(veiculo);
		}

	}

}
