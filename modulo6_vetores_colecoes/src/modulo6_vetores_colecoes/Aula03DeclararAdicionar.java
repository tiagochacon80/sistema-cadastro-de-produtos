package modulo6_vetores_colecoes;

public class Aula03DeclararAdicionar {

	public static void main(String[] args) {
		
		//Declarando e criando um array
		int[] valores = new int[4];
		//Associando valores as posi�oes
		valores[0] = 10;
		valores[1] = 15;
		valores[2] = 20;
		valores[3] = 25;
		
		//Pecorrendo o array com o for
		for(int i=0; i<valores.length; i++) {
			System.out.println(valores[i]);
		}

	}

}
