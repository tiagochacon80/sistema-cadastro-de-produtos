package modulo6_vetores_colecoes;

public class Aula04InverterValores {

	public static void main(String[] args) {


		int[] valores = new int[4];
		valores[0] = 5;
		valores[1] = 10;
		valores[2] = 15;
		valores[3] = 20;
		
		for(int i = 0; i < valores.length; i++) {
			System.out.println(valores[i]);
		}
		
		System.out.println();
		
		int[] valoresReverso = new int[valores.length];
		int posicaoMaxima = valores.length - 1;
		for(int i = posicaoMaxima; i >= 0; i--) {
			valoresReverso[posicaoMaxima - i] = valores[i];
		}
		
		for(int i = 0; i< valoresReverso.length; i++) {
			System.out.println(valoresReverso[i]);
		}
	}

}
