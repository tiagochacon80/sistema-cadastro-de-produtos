package modulo6_vetores_colecoes;

import java.util.HashMap;

public class Aula06HashMap {

	public static void main(String[] args) {
		
		HashMap<String, String> capitalCities = new HashMap<>();
		
		capitalCities.put("England", "London");
		capitalCities.put("Brazil", "Brasilia");
		capitalCities.put("USA", "Washington DC");
		capitalCities.put("Norway", "Oslo");
		
		System.out.println(capitalCities.get("Norway"));
		
		capitalCities.remove("England");
		
		System.out.println(capitalCities);

	}

}
