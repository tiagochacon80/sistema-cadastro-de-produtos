package modulo6_vetores_colecoes;

import java.util.ArrayList;

public class Aula01Listas {

	public static void main(String[] args) {
		ArrayList<String> colecao = new ArrayList<>();
		
		colecao.add("Carro");
		colecao.add("moto");
		colecao.add("Barco");
		colecao.add("Carro");
		//recuperando os valores do indices
		System.out.println(colecao.get(0));
		System.out.println(colecao.get(2));	
		
		System.out.println();
		System.out.println("For each");
		//Pecorrendo com um la�o nossa lista, com for each
		for(String veiculo :  colecao) {
			System.out.println(veiculo);
		}
		
		colecao.remove(0);
		
		System.out.println();
		System.out.println("For tradicional");
		//Pecorrendo com um la�o nossa lista, com for tradicional
		for(int i=0; i<colecao.size(); i++) {
			System.out.println(colecao.get(i));
		}

	}
}
