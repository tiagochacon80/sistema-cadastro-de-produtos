package streams_collectors;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Streams {

	public static void main(String[] args) {
		
		//Lista de string definida
		List<String> items = new ArrayList<String>();
		
		//4 objetos adicionados a lista
		items.add("um");
		items.add("dois");
		items.add("tr�s");
		items.add("quatro");
		
		//Stream de strings � obtida ao chamar o m�todo items.stream()
		Stream<String> stream = items.stream();
	}
}
