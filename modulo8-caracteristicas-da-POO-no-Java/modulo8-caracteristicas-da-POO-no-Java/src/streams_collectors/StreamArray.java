package streams_collectors;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamArray {

	public static void main(String[] args) {
		
		//Gerando uma stream a partir de arrays
		Stream<Integer> numbersFromValues = Stream.of(1, 2, 3, 4, 5);
			IntStream numbersFromArray = Arrays.stream(new int[] {1, 2, 3, 4, 4});

	}

}
