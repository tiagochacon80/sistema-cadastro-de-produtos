
public class Aula13Math {

	public static void main(String[] args) {
		//Arredondando numeros
		System.out.println(Math.round(14.5));
		System.out.println(Math.round(15.6));
		System.out.println(Math.round(13.4));
		//Buscando numeros aleatorios
		System.out.println(Math.random()+1);
		//Bucando um numero max em um range
		System.out.println(Math.max(2, 101));

	}

}
