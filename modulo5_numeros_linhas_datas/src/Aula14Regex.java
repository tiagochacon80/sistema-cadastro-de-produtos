import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Aula14Regex {

	public static void main(String[] args) {
		//╔ muito utilizado para localizar se a formatašao esta sendo usada corretamente
		//Expressoes regulares - Padrao de pesquisa
		String regex = "\\b([0-9]{3})\\.([0-9]{3})\\.([0-9]{3})\\-([0-9]{2})";		
		
		//Padrao foi definido
		Pattern pattern = Pattern.compile(regex);
		//Procura o padrao que foi definido anteriormente, retornando true caso encontre o padrao ou false se nao encontrar
		String cpf = "222.222.222-00";
		Matcher match = pattern.matcher(cpf);
		
		System.out.println(match.find());	
		
	}

}
