import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Aula12Calender {

	public static void main(String[] args) {

		Calendar data = Calendar.getInstance();
		
		System.out.println(data.getTime());
		//Adicionando um mes no calendario
		data.add(Calendar.MONTH, 1);
		System.out.println(data.getTime());
		//Adicionando dias ao calendario ou remover, basta colocar um - antes do numero
		data.add(Calendar.DAY_OF_MONTH, 5);
		System.out.println(data.getTime());
		//Alterando o mes atual pelo mes de agosto
		data.set(Calendar.MONTH, Calendar.AUGUST);
		System.out.println(data.getTime());
		//Formatando a data e hora
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		System.out.println(sdf.format(data.getTime()));

	}
}
