
public class Main {

	public static void main(String[] args) {

		//Wrapper
		Integer valor1 = 12;
		
		int valor2 = valor1;
		
		System.out.println(valor1);
		System.out.println(valor2);	
		
		
		//Double ou Integer
		//Quando usamo o Double do wrapper ele tem que ser definido com "d" no final = 12d
		Double valor3 = 12d;
		Double valor4 = 8d;
		
		//Caso seja uma divisao � melhor o Double
		Double resultado = valor3 / valor4;
		
		System.out.println(resultado);
	}

}
