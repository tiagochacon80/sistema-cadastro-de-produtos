
public class Aula07 {

	public static void main(String[] args) {
		char vetores[] = {'a', 'b', 'c'};
		System.out.println(vetores);
		//Convertendo char em String
		String texto = String.valueOf(vetores);
		System.out.println(texto);
		
		//Convertendo int em String
		long longConvertido = 12;		
		String valor = String.valueOf(longConvertido);
		
		System.out.println(valor);
		//Convertendo Double em String
		Double valorDouble = Double.valueOf(valor);
		System.out.println(valorDouble);

			
	}

}
