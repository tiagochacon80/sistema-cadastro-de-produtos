
public enum Aula11Cores {
	//A classe enum � muito utilizada para endere�os, nomes de ruas, avenidas, casos que sabemos que nao serao alterados facilmente
	VERMELHO("Vermelho"),
	AZUL("Azul"), 
	AMARELO("Amarelo"),
	VERDE("Verde");

	private String texto;
	Aula11Cores(String texto) {
		this.texto = texto;
	}
	public String getTexto() {
		return texto;
	}
	
}
