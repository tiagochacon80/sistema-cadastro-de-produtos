
public class Aula08 {

	public static void main(String[] args) {
		
		String texto = "Mentorama - Java - Curso";
		String texto1 = "Mentorama - JAVA";
		System.out.println(texto.charAt(0));
		
		//Fazendo compara�oes de string 0 ou a quantidade de caractares
		System.out.println(texto.compareTo(texto1));
		System.out.println(texto.compareToIgnoreCase(texto1));
		
		//Compara�ao booleane true or false
		System.out.println(texto.equals(texto1));
		System.out.println(texto.equalsIgnoreCase(texto1));
		
		//Texto come�a com "Men"
		System.out.println(texto.startsWith("Men"));
		//Texto finalixa com "Java"
		System.out.println(texto.endsWith("Java"));
		
		String[] textos = texto.split("-");
		for (String t: textos) {
			System.out.println(t.toUpperCase());
		}
		
		String vazia = "";
		System.out.println(vazia.isEmpty());
		//Imprime os caracteres nas posi�oes solicitadas
		System.out.println(texto.substring(1, 6));
	}
}
