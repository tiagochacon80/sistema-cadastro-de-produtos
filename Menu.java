import java.util.Scanner;

public class Menu {

    public static void main(String[] args) {

        int opcao;
        do {
            System.out.println("|       MENU  DE OPÇOES:          |");
            System.out.println("|O que voce deseja? (0 para sair):|");        
            System.out.println("|       1.Opçao 1                 |");
            System.out.println("|       2.Opçao 2                 |");
            System.out.println("|       3.Sair                    |");
            Scanner menu = new Scanner(System.in);        

            System.out.println("Selecione uma opçao: ");
            opcao = menu.nextInt();

            processar(opcao);
        } while (opcao != 3);
    }
        

    public static void processar(int opcao) {
        switch (opcao) {
            case 1:
                System.out.println("Opçao 1 Selecionada");
                break;
            case 2:
                System.out.println("Opçao 2 Selecionada");
                break;
            case 3:
                System.out.println("Você saiu do sistema");
                break;
            default:
                System.out.println("Seleçao invalida");
                break;                
            }
        }       
    }      
    
