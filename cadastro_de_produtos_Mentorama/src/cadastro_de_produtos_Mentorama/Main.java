package cadastro_de_produtos_Mentorama;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import db.EstoquesDB;
import db.PedidosVendaDB;
import db.ProdutosDB;
import db.UsuariosDB;
import models.Admin;
import models.Cliente;
import models.Estoque;
import models.PedidoVenda;
import models.Produto;
import models.Usuario;

public class Main {
	//Criando uma instancia da classe ProdutosDB
	static ProdutosDB produtosDB = new ProdutosDB();
	static UsuariosDB usuariosDB = new UsuariosDB();
	static EstoquesDB estoquesDB = new EstoquesDB();
	static PedidosVendaDB pedidosVendaDB = new PedidosVendaDB();
	

	public static void main(String[] args) throws Exception {
		
		Locale.setDefault(Locale.US);
		
		System.out.println("---- PEDIDO DE VENDAS ----");
		
		int option;
		
		do {
			System.out.println("1 - Cadastrar produto");
			System.out.println("2 - Listar produtos cadastrados");
			System.out.println("3 - Cadastrar usuario ADMINISTRADOR");
			System.out.println("4 - Cadastrar usuario CLIENTE");
			System.out.println("5 - Listar todos os usuarios cadastrados");
			System.out.println("6 - Cadastrar novo estoque de produtos");
			System.out.println("7 - Listar todos os produtos em estoques");
			System.out.println("8 - Criar pedido de venda");
			System.out.println("9 - Listar pedidos de venda");
			System.out.println("0 - Sair do menu");
			
			Scanner sc = new Scanner(System.in);
			System.out.println();
			System.out.print("Qual opera�ao voc� deseja executar: ");
			option = sc.nextInt();
			
			process(option);			
			
		}while(option != 0);		
	}
	
	public static void process(int option) throws Exception {
		switch (option) {
			case 1: {
				Scanner sc = new Scanner(System.in);
				
				System.out.print("Qual a descri�ao para o novo produto: ");
				String descricao = sc.nextLine();
				
				System.out.print("Qual o ID do novo produto: ");
				int id = sc.nextInt();
				
				System.out.print("Qual o pre�o: ");
				double preco = sc.nextDouble();
				
				System.out.print("Informe a data de validade: ");
				String dataString = sc.next(); 
				
				Date dataValidade = new SimpleDateFormat("dd/MM/yyyy").parse(dataString);
				
				Produto novoProd = new Produto(id, descricao, preco, dataValidade);
				
				produtosDB.addNovoProduto(novoProd);
				
				break;					
			}
			
			case 2: {
				List<Produto> listaDeProdutos = produtosDB.getProdutoList();
				
				for(Produto produto : listaDeProdutos) {
					System.out.println("--- ID " + produto.getId());
					System.out.println("--- Descri�ao " + produto.getDescricao());
					System.out.println("--- Pre�o: " + produto.getPreco());
					System.out.println("--- Data de validade " + produto.getDataValidade());
					System.out.println("------------------------------------------");
				}
				break;
			}
			
			case 3: {
				Scanner sc = new Scanner(System.in);
				
				System.out.print("Digite o nome do usuario ADMINISTRADOR: ");
				String nome = sc.nextLine();
				
				Admin novoAdmin = new Admin(nome);
				usuariosDB.addNovoUsuario(novoAdmin);
				break;
			}
			case 4: {
				Scanner sc = new Scanner(System.in);
				
				System.out.print("Digite o nome do usuario CLIENTE: ");
				String nome = sc.nextLine();
				
				Cliente novoCliente = new Cliente(nome);
				usuariosDB.addNovoUsuario(novoCliente);
				break;
			}
			case 5: {
				System.out.println("---------------------------------------");
				System.out.println("-----LISTANDO USUARIOS CADASTRADOS-----");
				System.out.println("---------------------------------------");
				for(Usuario usuario : usuariosDB.getUsuarioList()) {
					System.out.println("ID: " + usuario.getId());
					System.out.println("Nome: " + usuario.getNome());
					System.out.println("Tipo: " + usuario.getTipoUsuario());
					System.out.println("------------------------------------");
				}
				break;
			}
			case 6: {
				Scanner sc = new Scanner(System.in);
				System.out.println("---------------------------------------");
				System.out.println("----CADASTRANDO ESTOQUE DE PRODUTO-----");
				System.out.println("---------------------------------------");
				
				System.out.print("Informe o indentificador do estoque: ");
				String id = sc.next();
				
				System.out.print("Informe o produto que sera adicionado ao estoque: (Informe o ID): ");
				int produtoId = sc.nextInt();				
				
				Produto produto = produtosDB.getProdutoPorID(produtoId);
				System.out.println("PRODUTO ID: " + produto.getId());
				System.out.println("PRODUTO DESCRI�AO: " + produto.getDescricao());
				System.out.println("PRODUTO VALIDADE: " + produto.getDataValidade());
				
				System.out.print("Qual a quantidade de produtos a ser adicionada em estoque: ");
				int quantidade = sc.nextInt();
				
				Estoque novoEstoque = new Estoque(produto, quantidade, id);
				estoquesDB.addNovoEstoque(novoEstoque);
				break;
			}
			case 7: {
				System.out.println("---------------------------------------");
				System.out.println("-----LISTANDO ESTOQUES CADASTRADOS-----");
				System.out.println("---------------------------------------");
				for(Estoque estoque : estoquesDB.getEstoques()) {
					System.out.println("ID: " + estoque.getId());
					System.out.println("PRODUTO: " + estoque.getProduto().getDescricao());
					System.out.println("PRE�O: " + estoque.getProduto().getPreco());
					System.out.println("QUANTIDADE: " + estoque.getQuantidade());
					System.out.println("--------------------------------------");
				}
			}
			case 8: {
				Scanner sc = new Scanner(System.in);
				
				System.out.print("Inform o ID do cliente: ");
				int idCliente = sc.nextInt();
				
				Cliente cliente = (Cliente)usuariosDB.getUsuarioPorID(idCliente);
				System.out.println("ID: " + cliente.getId());
				System.out.println("NOME: " + cliente.getNome());
				System.out.println("TIPO: " + cliente.getTipoUsuario());
				System.out.println("------------------------------------------");
				
				System.out.print("Informe o ID do produto: ");
				int idProduto = sc.nextInt();
				Produto produto = produtosDB.getProdutoPorID(idProduto);
				System.out.println("PRODUTO ID: " + produto.getId());
				System.out.println("PRODUTO DESCRI�AO: " + produto.getDescricao());
				System.out.println("PRODUTO VALIDADE: " + produto.getDataValidade());
				System.out.println("------------------------------------------");
				
				System.out.print("Informe a quantidade: ");
				int quantidade = sc.nextInt();
				
				
				PedidoVenda novoPedido = new PedidoVenda(cliente, produto, quantidade);
				pedidosVendaDB.addNovoPedidoVenda(novoPedido);
				break;
			}
			case 9: {
				System.out.println("----------------------------------------");
				System.out.println("-------LISTANDO PEDIDOS DE VENDA--------");
				System.out.println("----------------------------------------");
				for(PedidoVenda pedidoVenda : pedidosVendaDB.getPedidoVenda()) {
					System.out.println("ID: " + pedidoVenda.getId());
					System.out.println("CLIENTE: " + pedidoVenda.getCliente().getNome());
					System.out.println("PRODUTO: " + pedidoVenda.getProduto().getDescricao());
					System.out.println("QUANTIDADE: " + pedidoVenda.getQuantidade());
					System.out.println("VALOR TOTAL: " + pedidoVenda.getValorTotal());
					System.out.println("---------------------------------------------------");
				}
				
				
				
				break;
			}
		}
	}
}
