package db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Produto;

public class ProdutosDB {	
	private Map<Integer, Produto> produtosMap = new HashMap<>();
	
	//Valida�ao para saber se o ID � o mesmo que o usuario solicita 
	public List<Produto> getProdutoList(){
		List<Produto> produtos = new ArrayList<>();
		for(Map.Entry<Integer, Produto> produto : produtosMap.entrySet()) {
			produtos.add(produto.getValue());
		}
		return produtos;
	}
	//Adicionando um novo produto a lista
	public void addNovoProduto(Produto produto) {
		produtosMap.put(produto.getId(), produto);
	}
	
	public Produto getProdutoPorID(int id) {
		return produtosMap.get(id);
	}

}
