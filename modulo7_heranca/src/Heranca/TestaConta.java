package Heranca;

public class TestaConta {

	public static void main(String[] args) {
		Conta contas[] = new Conta[3];
	
		ContaCorrente cc = new ContaCorrente(12212, 1668, "BB", 100, 1000);
		ContaPoupanca cp = new ContaPoupanca(13811, 1668, "BB", 100, 20, 0.05);
		ContaSalario cs = new ContaSalario(15548, 1668, "BB", 1000, 3);
		
		contas[0] = cc;
		contas[1] = cp;
		contas[2] = cs;
		
		cs.sacar(20);
		cs.sacar(15);
		cs.sacar(10);
		cs.sacar(10);
		cs.sacar(10);
		cs.sacar(10);		
		//cc.depositar(300);
		//cc.getSaldo();		
		
		System.out.println("Saldo de contas: ");
		for (Conta conta:contas) {
			System.out.println(conta);
			System.out.println("Saldo atual: R$ " + conta.getSaldo());	
			System.out.println("-------");
		}
	}	

}
